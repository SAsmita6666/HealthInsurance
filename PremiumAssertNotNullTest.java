package com.emids.premium.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.emids.premium.consumer.ConsumerDetailsWithHealthAndHabits;

public class PremiumAssertNotNullTest {

	
	ConsumerDetailsWithHealthAndHabits consumerDetailsWithHealthAndHabit = ConsumerDetailsWithHealthAndHabits.getInstanceOfConsumerDetails();
	
	String name = "abc";
	int age = 25;
	String gender = "male";
	boolean hypertension = false;
	boolean bloodPressure = true;
	boolean bloodSugar= false;
	boolean overweight = true;
	boolean smoking = true;
	boolean alcohol = false;
	boolean dailyExercise = true;
	boolean drug = false;
	
	
	public void setValuesToTheInstances() {

		consumerDetailsWithHealthAndHabit.setConsumer(name, gender, age);
		consumerDetailsWithHealthAndHabit.setHabits(dailyExercise, smoking, alcohol, drug);
		consumerDetailsWithHealthAndHabit.setHealth(hypertension, bloodPressure, bloodSugar, overweight);
		
	}
	
	
	
	@Test
	
	public void test() {
		
		PremiumAssertNotNullTest test = new PremiumAssertNotNullTest();
		test.setValuesToTheInstances();
		
		assertNotNull(consumerDetailsWithHealthAndHabit.getConsumer());
		assertNotNull(consumerDetailsWithHealthAndHabit.getHabits());
		assertNotNull(consumerDetailsWithHealthAndHabit.getHealth());
	}
	
	

	
}
