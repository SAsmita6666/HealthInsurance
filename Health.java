package com.emids.premium.consumer;

public class Health {

	private boolean hypertension;
	private boolean bloodPressure;
	private boolean bloodSugar;
	private boolean overweight;

	public Health(boolean hypertension, boolean bloodPressure, boolean bloodSugar, boolean overweight) {

		this.hypertension = hypertension;
		this.bloodPressure = bloodPressure;
		this.bloodSugar = bloodSugar;
		this.overweight = overweight;
	}

	public void getHealthInfo() {

		System.out.println("hypertension: " + hypertension + "bloodPressure: " + bloodPressure + "bloodSugar: "
				+ bloodSugar + "overweight: " + overweight);
	}

	public boolean isHypertension() {
		return hypertension;
	}

	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}

	public boolean isBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public boolean isBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	public boolean isOverweight() {
		return overweight;
	}

	public void setOverweight(boolean overweight) {
		this.overweight = overweight;
	}
	
}
