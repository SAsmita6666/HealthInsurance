package com.emids.premium.calculate;

import com.emids.premium.consumer.ConsumerDetailsWithHealthAndHabits;

public class PremiumCalculator  {

	int premium = 5000;
	private ConsumerDetailsWithHealthAndHabits consumerDetails;
	
	
	public PremiumCalculator(ConsumerDetailsWithHealthAndHabits consumerDetails) {
		this.consumerDetails = consumerDetails;
	}

	public boolean checkIfEligibleForBasePremium() {

		if (consumerDetails.getConsumer().getAge() <= 18) {
			return true;
		} else
			return false;

	}

	public int getTotalPremium() {

		if (checkIfEligibleForBasePremium()) {

			return premium;
		} else
			premium = premium+getPremiumBasedOnAge()+getPremiumBasedOnGender()+getPremiumBasedOnHealth()+getPremiumOnHabit();

		return premium;
	}
	
	
	

	public int getPremiumBasedOnAge() {

		int premiumOnAge = 0;

		if (consumerDetails.getConsumer().getAge() <= 25) {

			premiumOnAge = getTenPercOfPremiumValue();

		} else if (consumerDetails.getConsumer().getAge() <= 30) {

			premiumOnAge = getTenPercOfPremiumValue() + getTenPercOfPremiumValue();
		} else if (consumerDetails.getConsumer().getAge() >= 40) {
			premiumOnAge = getTwentyPercOfPremiumValue();
		}

		return premiumOnAge;
	}
	
	

	public int getPremiumBasedOnGender() {

		int premiumOnGender = 0;

		if (consumerDetails.getConsumer().getGender().equals("male")) {

			premiumOnGender = getTwoPercOfPremiumValue();
		}

		return premiumOnGender;
	}
	
	

	public int getPremiumBasedOnHealth() {

		int premiumOnHealth = 0;

		if (consumerDetails.getHealth().isBloodPressure()) {
			premiumOnHealth += getOnePercentPremiumValue();
		} else if (consumerDetails.getHealth().isBloodSugar()) {
			premiumOnHealth += getOnePercentPremiumValue();
		} else if (consumerDetails.getHealth().isHypertension()) {
			premiumOnHealth += getOnePercentPremiumValue();
		} else if (consumerDetails.getHealth().isOverweight()) {
			premiumOnHealth += getOnePercentPremiumValue();
		}

		return premiumOnHealth;
	}

	
	
	public int getPremiumOnHabit() {
		int premiumOnHabit = 0;

		if (consumerDetails.getHabits().isAlcohol()) {

			premiumOnHabit += getThreePercentPremiumValue();
		} else if (consumerDetails.getHabits().isDailyExercise()) {

			premiumOnHabit -= getThreePercentPremiumValue();
		} else if (consumerDetails.getHabits().isDrug()) {
			premiumOnHabit += getThreePercentPremiumValue();
		} else if (consumerDetails.getHabits().isSmoking()) {
			premiumOnHabit += getThreePercentPremiumValue();
		}
		return premiumOnHabit;

	}
	
	
	

	public int getTenPercOfPremiumValue() {

		return (premium * 10) / 100;
	}

	public int getTwentyPercOfPremiumValue() {

		return (premium * 20) / 100;
	}

	public int getTwoPercOfPremiumValue() {

		return (premium * 2) / 100;
	}

	public int getOnePercentPremiumValue() {
		return (premium * 1) / 100;
	}

	public int getThreePercentPremiumValue() {
		return (premium * 3) / 100;
	}

}
