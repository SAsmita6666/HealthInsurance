package com.emids.premium.consumer;

public class Habits {

	private boolean dailyExercise ;
	private boolean smoking;
	private boolean alcohol;
	private boolean drug;
	
	
	public Habits(boolean dailyExercis, boolean smoking,boolean alcohol, boolean drug ) {
		
		this.dailyExercise = dailyExercis;
		this.smoking = smoking;
		this.alcohol = alcohol;
		this.drug = drug;
	}
	
	
	public boolean isDailyExercise() {
		return dailyExercise;
	}


	public void setDailyExercise(boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}


	public boolean isSmoking() {
		return smoking;
	}


	public void setSmoking(boolean smoking) {
		this.smoking = smoking;
	}


	public boolean isAlcohol() {
		return alcohol;
	}


	public void setAlcohol(boolean alcohol) {
		this.alcohol = alcohol;
	}


	public boolean isDrug() {
		return drug;
	}


	public void setDrug(boolean drug) {
		this.drug = drug;
	}


	public void getHabits() {
		System.out.println("dailyexcecise: "+ dailyExercise+ "smoking: "+ smoking+ "alcohol: "+ alcohol+ "drug: "+ drug);
	}
	
}
