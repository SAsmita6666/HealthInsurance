package com.emids.premium.consumer;

public class ConsumerDetailsWithHealthAndHabits {

	private Consumer consumer;
	private Habits habits;
	private Health health;

	
	private ConsumerDetailsWithHealthAndHabits() {
		
	}
	
	
	private static class ConsumerDetailsWithHealthAndHabitsHelper{
		
		private static final ConsumerDetailsWithHealthAndHabits consumerDetails = new ConsumerDetailsWithHealthAndHabits(); 
	}
	
	public static ConsumerDetailsWithHealthAndHabits getInstanceOfConsumerDetails() {
		
		return ConsumerDetailsWithHealthAndHabitsHelper.consumerDetails;
	}
	
	public Consumer getConsumer() {
		return this.consumer;
	}

	public void setConsumer(String name, String gender, int age) {
		consumer = new Consumer(name, gender, age);
	}

	public Habits getHabits() {
		return this.habits;
	}
	
	public void  setHabits(boolean dailyExercise, boolean smoking, boolean alcohol, boolean drug) {
		habits = new Habits(dailyExercise, smoking, alcohol, drug);
	}


	public Health getHealth() {
		return this.health;
	}

	public void setHealth(boolean hypertension, boolean bloodPressure, boolean bloodSugar, boolean overweight) {
		health = new Health(hypertension, bloodPressure, bloodSugar, overweight);
	}
	
	
	


}
