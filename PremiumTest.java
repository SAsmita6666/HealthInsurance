package com.emids.premium.test;

import com.emids.premium.calculate.PremiumCalculator;
import com.emids.premium.consumer.ConsumerDetailsWithHealthAndHabits;

public class PremiumTest {
	
	

	public static void main(String[] args) {
		
		String name = "abc";
		int age = 25;
		String gender = "male";
		boolean hypertension = false;
		boolean bloodPressure = true;
		boolean bloodSugar= false;
		boolean overweight = true;
		boolean smoking = true;
		boolean alcohol = false;
		boolean dailyExercise = true;
		boolean drug = false;
		
		ConsumerDetailsWithHealthAndHabits consumerDetailsWithHealthAndHabit = ConsumerDetailsWithHealthAndHabits.getInstanceOfConsumerDetails();
		consumerDetailsWithHealthAndHabit.setConsumer(name, gender, age);
		consumerDetailsWithHealthAndHabit.setHabits(dailyExercise, smoking, alcohol, drug);
		consumerDetailsWithHealthAndHabit.setHealth(hypertension, bloodPressure, bloodSugar, overweight);
		
		PremiumCalculator calculator = new PremiumCalculator(consumerDetailsWithHealthAndHabit);
		System.out.println(calculator.getTotalPremium());
		
	}
}
